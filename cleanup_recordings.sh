#!/usr/bin/env bash
set -o errexit
set -o pipefail
if [[ "${TRACE-0}" == "1" ]]; 
then set -o xtrace;
fi

which psql-client

function from_dotenv {
if [ -f .env ]; then
    eval "$(
        cat .env | awk '!/^\s*#/' | awk '!/^\s*$/' | while IFS='' read -r line; do
            key=$(echo "$line" | cut -d '=' -f 1)
            value=$(echo "$line" | cut -d '=' -f 2-)
            echo "export $key=\"$value\""
        done
        )"
else
    echo '.env file is missing'
    exit 1
fi
}

while getopts ei:q:u:h opt;
do
    case ${opt} in

        i)  recording_id=${OPTARG};;
        q)  time_value=${OPTARG};;
        u)  time_unit=${OPTARG};;
        e)  use_env=true;;
        h)  echo \
            "
            cleanup_recording.sh is a small cli-tool to delete database entries of recordings in bbbatscale.

            ./cleanup_recording.sh [ARGUMENTS]

            -i <id> Delete recording with the provided id.
            -q      Time Value must be used with the -u flag for the time unit.
                    Value can be between 0 and 999.
            -u      Time unit must either be days, weeks, month, years. Must
            -e      Use local environment instead of .env file
            -h      display the help menu"
            exit 0;;
    esac
done

if [[ -z "${use_env+x}" ]]; then
    from_dotenv
else 
    echo "using variables from the environment"
fi

while read var; do
  [ -z "${!var}" ] && { echo "$var is empty or not set. Exiting.."; exit 1; }
done << EOF
BBBATSCALE_DB_USER
BBBATSCALE_DB_NAME
BBBATSCALE_DB_HOST
EOF

if [[ -z "${time_value+x}" ]] && [[ -z "${time_unit+x}" ]] && [[ -n "${recording_id+x}" ]]; then
    # check for correct id string
    if [[ ! $recording_id =~ ^[a-z0-9]{41}-[0-9]{14} ]]; then
        echo "You didn't provide a valid id. Please try again."
        exit 1
    fi

    read -r -p "Are you sure you wanna delete recording with the id: ${recording_id}? " response
    response=${response,,}    # (optional) move to a new line
    if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
        echo psql -U ${BBBATSCALE_DB_USER} -d ${BBBATSCALE_DB_NAME} -h ${BBBATSCALE_DB_HOST}  -c 'select * from core_meeting where replay_id = '${recording_id}';';
        echo "Recording deleted"
    fi
elif [[ -n "${time_value+x}" ]] && [[ -n "${time_unit+x}" ]]; then

    # check for allowed time quanitit
    if [[ ! $time_value =~ ^[0-9]{1,3} ]]; then
        echo "The quantity for the provided time value is out of range. Provided quantity can be in the range of 0-999."
    fi

    # check for allowed time unit
    if [[ $time_unit -ne "days" ]]||[[ $time_unit -ne "weeks" ]]||[[ $time_unit -ne "weeks" ]]||[[ $time_unit -ne "weeks" ]]||[[ $time_unit -ne "month" ]]||[[ $time_unit -ne "years" ]]; then
        echo "You didn't provide a valid time unit. time unit must be either days, weeks, month or years."
        exit 1
    fi
    echo psql -U ${BBBATSCALE_DB_USER} -d ${BBBATSCALE_DB_NAME} -h ${BBBATSCALE_DB_HOST}  -c 'select * from core_meeting where started < now() - interval '${time_value} ${time_unit}';';
else
    echo "You have to provide an id or a time quantity and a time unit."
    exit 1
fi