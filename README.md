# bbbatscale-recording-cleanup

This is a small script to assist in recording deletion 


## Usage
```bash
./cleanup_recording.sh [ARGUMENTS]

-i <id> Delete recording with the provided id
-q      Time Value must be used with the -u flag for the time unit
-u      Time unit must either be days, weeks, month, years
-e      Use local environment instead of .env file
-h      display the help menu
```